def test1():
    import mnist_loader
    import network

    a, b, c = mnist_loader.load_data_wrapper()

    net = network.Network([ 784, 30, 10 ])
    net.SGD(list(a), 31, 10, 3, test_data=list(c))

def test2():
    import mnist_loader
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

    import network2
    net = network2.Network([784, 30, 30, 10], cost=network2.CrossEntropyCost)
    net.SGD(list(training_data), 100, 10, 5.0,
        lmbda = 5.0,
        evaluation_data=list(validation_data),
        monitor_evaluation_accuracy=True,
        monitor_evaluation_cost=True,
        monitor_training_accuracy=True,
        monitor_training_cost=True,
        early_stopping=0,
        eta_schedule=(5, 10))

test2()
